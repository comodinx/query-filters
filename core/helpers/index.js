'use strict';

const objToString = Object.prototype.toString;
const hasOwnProperty = Object.prototype.hasOwnProperty;
const isArray = Array.isArray;

function isObject (value) {
    const type = typeof value;

    return value != null && (type == 'object' || type == 'function');
}

function isSymbol (value) {
    return typeof value === 'symbol' || (typeof value === 'object' && objToString.call(value) === '[object Symbol]');
}

function isFunction (value) {
    return isObject(value) && objToString.call(value) === '[object Function]';
}

function each (list, iterator) {
    if (isArray(list)) {
        return list.forEach(iterator);
    }
    return Object.keys(list).forEach(key => iterator(list[key], key, list));
}

function reduce (list, iterator, carry) {
    if (isArray(list)) {
        return list.reduce(iterator, carry);
    }

    Object.keys(list).forEach(key => {
        carry = iterator(carry, list[key], key, list)
    });
    return carry;
}

function invert (object, multiValue) {
    let index = -1;
    let props = Object.keys(object);
    let length = props.length;
    let result = {};

    while (++index < length) {
        const key = props[index];
        const value = object[key];

        if (multiValue) {
            if (hasOwnProperty.call(result, value)) {
                result[value].push(key);
            } else {
                result[value] = [key];
            }
        }
        else {
            result[value] = key;
        }
    }
    return result;
}

module.exports = {
    each,
    reduce,
    invert,
    isArray,
    isObject,
    isSymbol,
    isFunction
};